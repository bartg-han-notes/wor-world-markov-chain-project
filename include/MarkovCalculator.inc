#include "MarkovCalculator.hpp"

template< class T, std::size_t M, std::size_t N >
Matrix<T, 1, N> CalculateSteadyState(Matrix<T, M, N>& transitionMatrix, size_t startStateIndex)
{
    Matrix<T, 1, N> currentState(0);
    currentState.at(0, startStateIndex) = 1;
    int iterations = 0;

    while (true)
    {
        iterations++;
        Matrix<T, 1, N> tempState = currentState * transitionMatrix;
        // std::cout << "Iteration " << iterations << ": " << tempState << std::endl;
        if (equals(tempState, currentState, static_cast<T>(0.004)))
        {
            std::cout << "Steady state found after " << iterations << " iterations" << std::endl;
            return tempState;
        }
        currentState = tempState;
    }
}

template< class T, std::size_t M, std::size_t N >
Matrix<T, 1, N> CalculateNStepsAhead(Matrix<T, M, N>& transitionMatrix, size_t nDays, size_t startStateIndex)
{
    Matrix<T, 1, N> currentState(0);
    currentState.at(0, startStateIndex) = 1;

    while (nDays > 0)
    {
        nDays--;

        currentState = currentState * transitionMatrix;
    }

    return currentState;
}