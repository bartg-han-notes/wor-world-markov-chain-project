#pragma once

#include "matrix/Matrix.hpp"

template< class T, std::size_t M, std::size_t N >
Matrix<T, 1, N> CalculateSteadyState(Matrix<T, M, N>& transitionMatrix, size_t startStateIndex = 0);

template< class T, std::size_t M, std::size_t N >
Matrix<T, 1, N> CalculateNStepsAhead(Matrix<T, M, N>& transitionMatrix, size_t nDays = 3, size_t startStateIndex = 0);

#include "MarkovCalculator.inc"