#include <iostream>
#include "matrix/Matrix.hpp"
#include "MarkovCalculator.hpp"

int main(int, char**)
{
    // Matrix<float, 3, 3> transitionMatrix = {
    //     {.90f, .08f, .02f},
    //     {.00f, .40f, .60f},
    //     {.70f, .05f, .25f}
    // };
    Matrix<float, 3, 3> transitionMatrix = {
        {.80f, .10f, .10f},
        {.03f, .95f, .02f},
        {.20f, .05f, .75f}
    };

    std::cout << "Transition matrix:\n" << transitionMatrix << std::endl;

    std::cout << "State after 3 days:\n" << CalculateNStepsAhead(transitionMatrix, 3, 1) << std::endl;

    std::cout << CalculateSteadyState(transitionMatrix, 1) << std::endl;
}